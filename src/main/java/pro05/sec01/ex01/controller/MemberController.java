package pro05.sec01.ex01.controller;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pro05.sec01.ex01.dao.MemberDAO;
import pro05.sec01.ex01.entity.MemberDTO;

@WebServlet("/member")
public class MemberController extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		MemberDAO dao = MemberDAO.getInstance();
		
		List<MemberDTO> members = dao.selectMembers();

		System.out.println(members.size());
		
		resp.setContentType("text/html; charset=UTF-8");
		PrintWriter out = resp.getWriter();
		
		out.print("<table>");
		out.print("<th>id</th>");
		out.print("<th>pwd</th>");
		out.print("<th>name</th>");
		out.print("<th>email</th>");
		out.print("<th>joinDate</th>");
		for (int i = 0; i < members.size(); i++) {
			MemberDTO member = (MemberDTO) members.get(i);
			String id = member.getId();
			String pwd = member.getPwd();
			String name = member.getName();
			String email = member.getEmail();
			Date joinDate = member.getJoinDate();
			out.print("<tr><td>" + id + "</td><td>" + pwd + "</td><td>" + name + "</td><td>" + email + "</td><td>"
					+ joinDate + "</td></tr>");
		}
		out.print("</table>");
	}
}
