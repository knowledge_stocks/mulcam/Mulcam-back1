package pro05.sec01.ex01.login;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pro05.sec01.ex01.dao.MemberDAO;
import pro05.sec01.ex01.entity.MemberDTO;

@WebServlet("/loginPage")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doHandle(req, resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doHandle(req, resp);
	}

	private void doHandle(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setContentType("text/html;charset=utf-8");
		PrintWriter out = resp.getWriter();
		String user_id = req.getParameter("user_id");
		String user_pwd = req.getParameter("user_pwd");

		MemberDTO memberVO = new MemberDTO(user_id, user_pwd, null, null, null);
		memberVO.setId(user_id);
		memberVO.setPwd(user_pwd);
		MemberDAO dao = MemberDAO.getInstance();
		boolean result = dao.isExisted(memberVO);

		// 회원이면
		if (result) {
			HttpSession session = req.getSession();
			session.setAttribute("isLogon", true);
			session.setAttribute("login.id", user_id);
			session.setAttribute("login.pwd", user_pwd);

			out.print("<html><body>");
			out.print("안녕하세요 " + user_id + "님!!!<br>");
			out.print("<a href='show'>회원정보보기</a>");
			out.print("</body></html>");
		} else {
			out.print("<html><body>회원 아이디가 틀립니다.");
			out.print("<a href='login.html'> 다시 로그인하기</a>");
			out.print("</body></html>");
		}
	}

}
