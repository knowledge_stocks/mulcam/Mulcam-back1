package pro05.sec01.ex01;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/second", initParams = { @WebInitParam(name = "dbName", value = "mydb"),
		@WebInitParam(name = "dbPwd", value = "1234") })
public class SecondServlet extends HttpServlet {

	@Override
	public void init() throws ServletException {
		// 처음 호출할 때만 실행됨
		System.out.println("init");
	}

	// init(config)을 오버라이딩하면 init()는 무시된다.
	@Override
	public void init(ServletConfig config) throws ServletException {
		// 나중에 getInitParameter 메소드로 값을 가져오려면 반드시 부모의 init을 호출해주어야 한다.
		super.init(config);
		
		Enumeration<String> e = config.getInitParameterNames();
		while (e.hasMoreElements()) {
			String name = e.nextElement();
			System.out.printf("%s: %s\n", name, config.getInitParameter(name));
		}
		System.out.println();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("get");
	}

	@Override
	public void destroy() {
		// 서버가 종료될 때 실행됨
		System.out.println("destroy");
	}
}
