package pro05.sec01.ex01.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import pro05.sec01.ex01.entity.MemberDTO;

public class MemberDAO {
	private static MemberDAO instance = null;

	private DataSource dataFactory;

	private MemberDAO() {
		// tomcat에 설정한 connectionPool 사용하는 방법
		Context ctx;
		try {
			ctx = new InitialContext();
			Context cnvContext = (Context) ctx.lookup("java:/comp/env");
			dataFactory = (DataSource) cnvContext.lookup("jdbc/mysql");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	};

	public static MemberDAO getInstance() {
		if (instance == null) {
			synchronized (MemberDAO.class) {
				if (instance == null) {
					instance = new MemberDAO();
				}
			}
		}
		return instance;
	}

	public int insertMember(MemberDTO member) {
		int result = -1;

		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = dataFactory.getConnection();

			String id = member.getId();
			String pwd = member.getPwd();
			String name = member.getName();
			String email = member.getEmail();

			String sql = "INSERT INTO t_member " + "(id, pwd, name, email) " + "VALUES(?, ?, ?, ?)";

			// ?로 나중에 값을 넣을 때는 prepareStatement를 사용하면 된다.
			st = conn.prepareStatement(sql);
			st.setString(1, id);
			st.setString(2, pwd);
			st.setString(3, name);
			st.setString(4, email);

			// 변경된 row의 수를 반환한다.
			result = st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (st != null) {
					st.close();
					st = null;
				}
				if (conn != null) {
					conn.close();
					conn = null;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	public List<MemberDTO> selectMembers() {
		List<MemberDTO> members = new ArrayList<MemberDTO>();

		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			conn = dataFactory.getConnection();

			String sql = "SELECT * " + "FROM t_member";

			st = conn.createStatement();
			rs = st.executeQuery(sql);

			while (rs.next()) {
				String id = rs.getString("id");
				String name = rs.getString("name");
				String email = rs.getString("email");
				Date joinDate = rs.getDate("joinDate");

				members.add(new MemberDTO(id, null, name, email, joinDate));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
					rs = null;
				}
				if (st != null) {
					st.close();
					st = null;
				}
				if (conn != null) {
					conn.close();
					conn = null;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return members;
	}

	public boolean isExisted(MemberDTO member) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = dataFactory.getConnection();

			String id = member.getId();
			String pwd = member.getPwd();

			String query = "select * from t_member where id=? and pwd=?";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, id);
			pstmt.setString(2, pwd);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
