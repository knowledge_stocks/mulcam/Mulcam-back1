﻿CREATE DATABASE mydb;
USE mydb;

CREATE TABLE mydb.t_member (
  id VARCHAR(10) NOT NULL DEFAULT '',
  pwd VARCHAR(10) DEFAULT NULL,
  name VARCHAR(50) DEFAULT NULL,
  email VARCHAR(50) DEFAULT NULL,
  joinDate DATE DEFAULT (current_date()),
  PRIMARY KEY (id)
);

INSERT INTO t_member
VALUES('hong', '1212', '홍길동', 'hong@hooolaa.com', current_date());

INSERT INTO t_member
VALUES('lee', '1212', '이순신', 'lee@test.com', current_date());

INSERT INTO t_member
VALUES('kim', '1212', '김유신', 'kim@jjwebbb.com', current_date());

SELECT * FROM t_member;
